# MagiCalc (w/UrbanCode Deploy)

## A totally over-engineered and very basic calculator 

The purpose of MagiCALC is to demonstrate various use cases in DevSecOps with GitLab

_Note: The application code within this project contains deliberate vulnerabilities and anti patterns intended to be exposed via security scans._

MagiCALC is a simple calculator application which leverages a React frontend for the user interface and a NodeJS/Express API backend to perform the calculations. These separate apps are stored as a monorepo and the GitLab CI file is configured to build the apps separately and store them as individual images within the container repo. 

View the issues included in this project for a series of excercises on adding security scans and more functionalty to the app. 

### See Also
 - [How to integrate a GitLab CI/CD Pipeline with IBM UrbanCode Deploy](https://community.ibm.com/community/user/wasdevops/blogs/randy-langehennig1/2021/06/09/how-to-integrate-a-gitlab-cicd-pipeline-with-ibm-u?CommunityKey=0ab505af-8e12-4199-843b-0dbbb3848f0e)
- [Build a pipeline with GitLab CI, IBM Dependency Based Build, and IBM UrbanCode Deploy](https://developer.ibm.com/tutorials/build-a-pipeline-with-gitlab-ci-dbb-and-ucd/)

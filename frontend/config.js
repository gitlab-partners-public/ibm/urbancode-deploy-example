const api_config = function(){
  if (process.env.NODE_ENV == "production") {
    return 'http://169.48.27.123:8383'
  } else{
    return 'http://169.48.27.123:8383'
  } 
}();

const GCP_KEY = null;
const AWS_KEY = null;
const AWS_SECRET = null;

export {
  api_config, AWS_KEY, GCP_KEY, AWS_SECRET
}
